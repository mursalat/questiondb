
/*
 * questiondb routes
 */

var qdb_m = require('../models')
var config = require('../config')
var mongoose = require('mongoose')
function authinfo (request){
  if (typeof request.user != "undefined" && request.user.email.length > 5)
  {
    return { name: request.user.name, authenticated: true, id: request.user._id+"" }
  }
  else {
    return { authenticated: false, name: "", id:""}
  }
}

function numb_aq (session){
  if (typeof session != "undefined" && typeof session.assignment_questions != "undefined"){
    return session.assignment_questions.length
  }
  else return 0
}

exports.list_tags = function(req, res){
  res.render('question_tag', {user: authinfo(req) , meta: { numb_aq: numb_aq(req.session)}, title: "Question Topics" })
};

//todo assignment create

//todo implement search features
exports.search_question= function(req, res){
  var searchtext = req.params.searchtext;
  if (typeof searchtext == 'undefined') {
    res.render('question_search', { user: authinfo(req) , meta: { numb_aq: numb_aq(req.session)}, title: "Search", searchtext: "", results: null })
  }
  else {
    //process search
    //searchtext = searchtext.replace('<', '')
    res.render('question_search', { user: authinfo(req) , meta: { numb_aq: numb_aq(req.session)}, searchtext: searchtext})
  }
}

//todo: get number of tries, calculate new average tries required
exports.ajax_average_tries = function(req,res){
  var num_tries = req.param.numtries;
  if (typeof num_tries != 'undefined'){
    var qdifficultym = new qdb_m.QuestionDifficulty();
    //create or update existing average tries required.
  }
}

exports.show_form = function(req, res){ //show form or load question than show form.
  var ques = new qdb_m.MCQuestion();
  var qid = req.params.qid;
  var is_new = true;
  
  if (typeof qid != 'undefined'){
    qdb_m.MCQuestion.findOne({ '_id': qid }, 'question answers tags', function (err, quest) {
      if (err) console.log("¨QDB ERR: failed to fetch question");
      ques= quest;
      res.render('question_form', { user: authinfo(req) , meta: { numb_aq: numb_aq(req.session)}, state: 'update', title: 'Update Question', question: ques , errors: []});
      //console.log('%s', quest._id) // Space Ghost is a talk show host.
    })
  }
  else{// new question
    ques.answers.push({});
    ques.answers.push({});
    ques.answers.push({});
    res.render('question_form', { user: authinfo(req) , meta: { numb_aq: numb_aq(req.session)}, state:  'create' , title: 'Create Question', question: ques , errors: []});
  }
  
};
function handleError(req,res, question, err){
  var proc_err = [];
  for (var f in err.errors){
          //console.log("error is: ", err.errors[f].message);
          proc_err.push(err.errors[f].message);
        }
        //var mcq = new qdb_m.MCQuestion({});
        //console.log("--------------------> question: ", question);
        if(err){
          return res.render('question_form', {
            title: "Create Question",
            user: authinfo(req) , meta: { numb_aq: numb_aq(req.session)},
            state: req.body.state,
            question: question,
            errors: proc_err,
          });
        } 
}

exports.process_form = function(req, res){ // either update or create
  var tmp_st = [];
  var proc_err = [];
  var qid = null;
  var state = "create";
  
  if (req.body.state == "update") {
    qid = req.body.qid;
    //console.log("QDB INF: Question Update: ID#"+ qid);
  }
  req.body.tags.split(',').forEach(function(st){
    tmp_st.push(st.replace(/^\s+|\s+$/g, ""));
  });
  
  var question = new qdb_m.MCQuestion({
        question: req.body.question,
        solution: req.body.solution,
        votes_up: 0,
        votes_down: 0,
        tags: tmp_st,
        answers: [],
  });
  
  if (qid != null){
    question._id = qid;
  }
  
  
  var k = 0;
  req.body.answer.forEach(function(ans){
    k++;
    if (ans.length)
      question.answers.push({answer: ans, is_correct: k == req.body.correct_answer ? true : false });
  });
  

  if (qid == null) {
    question.created_by= req.user._id;
    question.save(function(err, ques){
      //console.log("save");
      if(err==null) {
        res.redirect("/qv/"+ques._id);
      }
      else return handleError(req, res, question, err);
    });
  }
  else {
    qdb_m.MCQuestion.findOne({'_id':qid}, "_id created_by", function(err, ques){
      if (ques.created_by==req.user._id+""){
        if (err!=null)
          return handleError(req, res, question, err);
        question.validate(function(err){
          if (err == null){
            qdb_m.MCQuestion.findByIdAndUpdate(qid, { $set: question}, function (err, ques) {
              console.log("updated err-> ", err)
              if(err==null) {
                  res.redirect("/qv/"+ques._id);
              }
              else return handleError(req, res, question, err);
            });
          }
          else return handleError(req, res, question, err)
        })
      }
      else{
        res.send("For some reason you seemed to be updating a question not belonging to you.")
      }
    })
  }
};

exports.question_my = function(req, res){
  var page_number = parseInt(req.params.pnum);
  if (isNaN(page_number)){
    page_number = 0;
  }
  qdb_m.MCQuestion.count({created_by: req.user._id}, function(err, c){
    var numb_pages = Math.ceil(c/config.number_of_questions_per_page);
    qdb_m.MCQuestion.find({ created_by: req.user._id}, function(err, docs){
      if(err!= null)
        console.log("mcq find mongoose err: ",err);
      else
        res.render('question_list', {
            user: authinfo(req) , 
            meta: { numb_aq: numb_aq(req.session)}, 
            title:'Questions by '+ req.user.name, 
            questions: docs,
            pagination: { exist: c>config.number_of_questions_per_page, numb_pages: numb_pages , current_page: page_number }, 
          })
    }).limit(config.number_of_questions_per_page).skip(page_number*config.number_of_questions_per_page).sort({"create_on":"desc"});
  });
}




exports.list = function(req, res){
  var tmp_st=[];
  var search_tags=req.params.tag.split(';');
  var page_number = parseInt(req.params.pnum);
  if (isNaN(page_number)){
    page_number = 0;
  }
  
  var numb_questions = 0;
  //console.log("########################\nthe tags are:  ",req.session.auth);
  search_tags.forEach(function(st){
    tmp_st.push(st.replace(/^\s+|\s+$/g, ""));
  });
  //IMPORTANT TODO : count optimization here required
  qdb_m.MCQuestion.count({tags : {$all : tmp_st}}, function(err, c){
    var numb_pages = Math.ceil(c/config.number_of_questions_per_page);
    qdb_m.MCQuestion.find({ tags : {$all : tmp_st}}, function(err, docs){
      /*docs.forEach(function(d){
        console.log(d);
      });*/
      if(err!= null)
        console.log("mcq find mongoose err: ",err);
      else
        res.render('question_list', {
          user: authinfo(req) , 
          meta: { numb_aq: numb_aq(req.session)}, 
          title:'Questions for: #'+tmp_st.join(" #"), 
          pagination: { exist: c>config.number_of_questions_per_page, numb_pages: numb_pages , current_page: page_number }, 
          questions: docs
        })
    }).limit(config.number_of_questions_per_page).skip(page_number*config.number_of_questions_per_page).sort({"create_on":"desc"});
  });
};

exports.view = function(req, res){ // by tag, by me
  qdb_m.MCQuestion.findOne({_id: req.params.qid}, function (err, q){
    //console.log(q);
    if(err != null)
      console.log("mcq find err: ", err)
    else
      res.render('question_view', {user: authinfo(req) , meta: { numb_aq: numb_aq(req.session) }, title: q.tags.join (" ").replace("#", ""), question: q})
  })
  //res.render('question_view', {title:'question list'})
};
exports.assignment_view_s = function(req,res){ // view single assignment
  var aid = req.params.aid;
  qdb_m.Assignment.findOne({_id: aid}, function(err, ass){
    if (err == null){
      var qs = ass.questions
      qdb_m.MCQuestion.find({_id: {$in: qs}}, function(err, ques){
        res.render('question_list', {
          user: authinfo(req) , 
          meta: { numb_aq: numb_aq(req.session)}, 
          title: ass.name, 
          questions: ques,
          hide_qoptions : true
        })
      })
    }
    else
      console.log(err)
  })
}
exports.assignment_view = function (req, res){ // with id from db, or from session
  if (typeof req.session.assignment_questions == "undefined" || req.session.assignment_questions.length < 1  && typeof req.params.aid == "undefined"){
    res.send("Add questions to assignment to view it.")
  }
  var qids = []
  req.session.assignment_questions.forEach(function(d){
    qids.push(mongoose.Types.ObjectId(d))
  })
  
  if (req.method == "POST"){
    var assignment = new qdb_m.Assignment({
          name: req.body.assignmentname,
          created_by :  mongoose.Types.ObjectId(req.user._id),
          questions: qids,
          name_required: false, // todo : implement
          tell_correct: true, // todo : implement
    });
    assignment.save(function(err, a){
      req.session.assignment_questions = []
      if (err == null)
        res.redirect("/a/v/"+a._id)
      else
        console.log(err)
    })
  }
  else {
    qdb_m.MCQuestion.find({'_id': { $in: qids}}, function(err, docs){
        res.render('assignment_view', {
          user: authinfo(req) , 
          meta: { numb_aq: numb_aq(req.session)}, 
          title: "New Assignment", 
          questions: docs,
          hide_qoptions : true
        })
    })
  }
}


exports.ajax_assignment_add_question = function(req, res){
  if (typeof req.session.assignment_questions == "undefined"){
    req.session.assignment_questions= []
  }
  var exists = false
  req.session.assignment_questions.forEach(function(k){
    if (k == req.params.qid){
      exists = true
    }
  })
  if (!exists){
    req.session.assignment_questions.push(req.params.qid)
    res.send("success|Question added for assignment.")
  }
  else 
    res.send("warning|You already added that question to the assignment.")
}

exports.ajax_assignment_clear = function(req, res){
  req.session.assignment_questions=[]
  res.send("success|Cleared active assignment.")
}

exports.assignment_my = function(req, res){
  qdb_m.Assignment.find({ created_by: req.user._id}, function(err, docs){
    console.log("assignments:::===>> ", docs)
    res.render('assignment_list', {
      user: authinfo(req) , 
      meta: { numb_aq: numb_aq(req.session)}, 
      title: "My Assignments", 
      assignments: docs,
    })
  })
}

exports.upload_file = function(req,res){ //todo save in user model uploaded files for the user.
  res.writeHead(200, {"Content-Type":"application/json"});
  var json = JSON.stringify({
    "uploaded": 1,
    "filename" : req.file.originalname,
    "url":"/images/upload/"+req.file.filename
  });
  res.end(json);
}

//exports.assignment_pdf = function ()

//exports.quiz_view = function(req, res){
//  res.render('index', {title:'quiz'})  
//};