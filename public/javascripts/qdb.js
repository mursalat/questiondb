/*
var notifications = []
-if (typeof messages != "undefined"){
-each msg in messages
notifications.append({"type": msg.type, "message": msg.message})
-}
$(document).ready(function(){
for (notif in notifications){
  $.notify({"message": notif.message},{ type:notif.type, placement: {from: "bottom", align: "right"}})
}
});*/
function process_ajax_ret(dat, urc){
	// ajax data is formatted as : "action_flag|[options]"
	//default is notify action
	k = dat.split("|")
	$.notify({"message": k[1]},{ type:k[0], placement: {from: "bottom", align: "right"}})//notify 3.1.3
	if(parseInt($("#assignmentsnumb").text().replace("Assignments (", "").replace(")", "")) == 0 || (urc.startsWith("/a/c") && k[0]=='success')){
	    location.reload()
	}
	if (urc.startsWith("/a/a/") && k[0]=='success'){
	    v = parseInt($("#assignmentsnumb").text().replace("Assignments (", "").replace(")", ""))+1;
	    $("#assignmentsnumb").text("Assignments ("+v+")")
	}
}
function ajax_call(urc){
	console.log(urc);
	$.ajax(urc)
	.done(function(data){
		process_ajax_ret(data,urc)
	})
	.fail(function(){
		$.notify({"message": "There was a issue connecting to server, is your internet working?"},{ type:"error", placement: {from: "bottom", align: "right"}})//notify 3.1.3
	})
}