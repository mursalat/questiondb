var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var User = require("./models").User
var config = require("./config")

function google(passport){
    passport.use(new GoogleStrategy({
        clientID        : config.googleAuth.clientID,
        clientSecret    : config.googleAuth.clientSecret,
        callbackURL     : config.googleAuth.callbackURL,

    },
    function(token, refreshToken, profile, done) {

        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {

            // try to find the user based on their google id
            User.findOne({ 'google.id' : profile.id }, function(err, user) {
                if (err)
                    return done(err);

                if (user) {

                    // if a user is found, log them in
                    return done(null, user);
                } else {
                    // if the user isnt in our database, create a new user
                    var newUser          = new User();

                    // set all of the relevant information
                    newUser.name = profile.displayName;
                    newUser.google.id    = profile.id;
                    newUser.google.token = token;
                    newUser.google.name  = profile.displayName;
                    newUser.google.email = profile.emails[0].value; // pull the first email

                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });

    }));
}
function facebook(passport){
    console.log("facebook strategy setup")
    passport.use('facebook', new FacebookStrategy({
        clientID        : config.facebookAuth.clientID,
        clientSecret    : config.facebookAuth.clientSecret,
        callbackURL     : config.facebookAuth.callbackURL,
        profileFields: ["id", "displayName", "emails"],
        enableProof: true
    },
    // facebook will send back the tokens and profile
    function(access_token, refresh_token, profile, done) {

    	console.log('profile', profile);

		// asynchronous
		process.nextTick(function() {

			// find the user in the database based on their facebook id
	        User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

	        	// if there is an error, stop everything and return that
	        	// ie an error connecting to the database
	            if (err)
	                return done(err);

				// if the user is found, then log them in
	            if (user) {
	                return done(null, user); // user found, return that user
	            } else {
	                // if there is no user found with that facebook id, create them
	                var newUser = new User();

					// set all of the facebook information in our user model
	                newUser.facebook.id    = profile.id; // set the users facebook id	                
	                newUser.facebook.token = access_token; // we will save the token that facebook provides to the user	                
	                //newUser.facebook.firstName  = profile.name.givenName;
	                //newUser.facebook.lastName = profile.name.familyName; // look at the passport user profile to see how names are returned
	                newUser.name = profile.displayName;
	                newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
	                newUser.email = profile.emails[0].value;

					// save our user to the database
	                newUser.save(function(err) {
	                    if (err)
	                        throw err;

	                    // if successful, return the new user
	                    return done(null, newUser);
	                });
	            }

	        });
        });

    }));
}
module.exports = {
    init : function(passport){
        passport.serializeUser(function(user, done) {
            //console.log('serializing user: ');console.log(user);
            done(null, user._id);
        });
        passport.deserializeUser(function(id, done) {
            User.findById(id, function(err, user) {
                //console.log('deserializing user:',user);
                done(err, user);
            });
        });
        console.log("initiate passport")
        
        //google(passport);
        facebook(passport)
    },
    isauthenticated: function(req,res, next){
        if (typeof req.user != "undefined" && req.user.email.length > 10)
            return next();
        res.redirect("/auth/")
    }
}

