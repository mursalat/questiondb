
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , questiondb = require('./routes/questiondb')
  , user = require('./routes/user')
  , http = require('http')
  , config = require('./config')
  , path = require('path');


var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var session = require("express-session");

var passport = require("passport");

  
var mongoose = require('mongoose');
mongoose.connect(config.mongo_uri);

var passport_auth = require("./sso")

var multer = require('multer');
var crypto = require('crypto');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, config.user_uploads)
  },
  filename: function (req, file, cb) {
    var hash = crypto.createHash('md5').update(file.fieldname + '-' + Date.now()).digest('hex');
    cb(null, hash+"."+file.originalname.split(".").pop(-1))
  }
})

var upload = multer({ storage:storage })

var app = express();



app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(session({
  secret: 'f0j34003iey7r08s', 
  name: "qdbc", 
  proxy: true, 
  resave: true, 
  cookie: { maxAge: 60000000 },
  saveUninitialized: true,
}));
app.use(passport.initialize());
app.use(passport.session())

passport_auth.init(passport);


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.set('x-powered-by', false)
app.use(express.favicon());
app.use(express.logger('dev'));

//app.use(express.bodyParser());
//app.use(express.methodOverride());
//app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.use(function(req, res, next){
  res.locals.session = req.session
  res.locals.message = {
    type: null,
    text: null
  }
  next()
})

app.get('/', questiondb.list_tags);
app.get('/users', user.list);

app.get('/s', questiondb.search_question); //todo implement search
app.get('/qc', passport_auth.isauthenticated, questiondb.show_form);
app.post('/qpf', passport_auth.isauthenticated, questiondb.process_form);
app.get('/ql', questiondb.list_tags); //todo catalogue 
app.get('/ql/:tag/:pnum', questiondb.list); //todo make last part depcreatecated.
app.get('/qv/:qid', questiondb.view); // view question
app.get('/qe/:qid', passport_auth.isauthenticated, questiondb.show_form);
app.get('/q/m/:pnum', passport_auth.isauthenticated, questiondb.question_my);
app.get('/auth', user.auth)

app.get('/a/v/:aid', questiondb.assignment_view_s)  // todo view assignment
app.get('/a/s', passport_auth.isauthenticated, questiondb.assignment_view)
app.post('/a/s/p', passport_auth.isauthenticated, questiondb.assignment_view)
app.get('/a/c', passport_auth.isauthenticated, questiondb.ajax_assignment_clear)
app.get('/a/a/:qid', passport_auth.isauthenticated, questiondb.ajax_assignment_add_question)
app.get('/a/m', passport_auth.isauthenticated, questiondb.assignment_my)

app.post('/ckup', upload.single('upload'), questiondb.upload_file)


app.get("/auth/facebook", passport.authenticate('facebook', {scope: "email"}));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { session: true, successRedirect: '/', failureRedirect: '/auth'}))

//todo add google auth here

app.get('/u/l', function(req, res){
  console.log("loging out: "+ req.user)
  req.session.destroy();
  req.logout();
  res.redirect("/");
})

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
