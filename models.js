var mongoose = require("mongoose")
var Schema = mongoose.Schema;

/* schemas */

/*
mongoose.model('User',{
        username: String,
    password: String,
    email: String,
    gender: String,
    address: String
});
*/

var userSchema = new Schema({
  email: String, // todo: add local authentication.
  password: String,
  name: String,
  facebook: {
    id : String,
    email: String,
    token: String,
    name: String,
  },
  google : {
    id : String, 
    email: String,
    token: String,
    name: String,
  },
  login_at: Date, 
  register_at : {type: Date, default:Date.now}
})

var questionSchema = new Schema({
    question: String,
    created_by :  mongoose.Schema.Types.ObjectId,
    solution: String, 
    create_on: {type: Date, default: Date.now },
    updated_on: {type: Date, default: Date.now},
    votes_up: Number,
    votes_down: Number, 
    tags: [String],
    answers: [{
      answer: {type: String},
      is_correct: {type: Boolean, default: false }
    }], 
})
var tagSchema = new Schema ({
    questions: [{ type: Schema.Types.ObjectId, ref: 'MCQuestion' }],
})
var assignmentSchema = new Schema ({
    name: {type:String, default:"untitled"},
    created_by :  mongoose.Schema.Types.ObjectId,
    questions: [{ type: Schema.Types.ObjectId, ref: 'MCQuestion' }],
    created_on : {type: Date, default: Date.now},
    password: { type: String, default: null },
    name_required: Boolean,
    tell_correct: Boolean,
    tags: {type: [String], index: true},
    hours_duration: Number
})

var assignmentResult = new Schema ({
  answer_by: {type:  mongoose.Schema.Types.ObjectId, ref: 'User'},
  assignment_start : Date,
  assignment: {type:  mongoose.Schema.Types.ObjectId, ref: 'Assignment'},
  answers: [{
    question: { type: Schema.Types.ObjectId, ref: 'MCQuestion'},
    is_correct: Boolean
    }] , 
  answer_number: Number ,
  client_info : String
})

//newAve = ((oldAve*oldNumPoints) + x)/(oldNumPoints+1)
var questionDifficultySchema = new Schema({
  for_mcquestion: {type: Schema.Types.ObjectId, ref: 'MCQuestion'},
  current_average_tries: Number,
  number_of_tries_count: Number,
})

var tMCQuestion = mongoose.model('MCQuestion', questionSchema);
var tAssignment = mongoose.model('Assignment', assignmentSchema);
//var tTag = mongoose.model('QTag', tagSchema);
var tQuestionDifficulty = mongoose.model ('QuestionDifficulty', questionDifficultySchema)
var tUser = mongoose.model("User", userSchema)

/* validations */


tMCQuestion.schema.path('question').validate(function (value) { //validate question 
  return (value.length > 0 ? true : false );
}, 'Question can\'t be empty.');

tMCQuestion.schema.path('answers').validate(function (value) { // 
  return (value.length > 1 ? true : false );
}, 'Question must have a minimum of 2 answers.');

tMCQuestion.schema.path('tags').validate(function (value) { // 
  var retval = false;
  value.forEach(function(v){
    v = v.replace(/\s/g, '') ;
    if (typeof v != "undefined" && v!= "" && v!= null){
      retval = true;
    }
  })
  return retval;
}, 'Atleast 1 tag must be provided.');

tMCQuestion.schema.path('answers').validate(function (value) { // 
  var rt = false;
  value.forEach(function(ans){
      if(ans.is_correct){ rt = true};
  });
  return rt;
}, 'Select the correct answer.');

exports.MCQuestion = tMCQuestion;
exports.Assignment = tAssignment;
exports.QuestionDifficulty = tQuestionDifficulty;
exports.User = tUser






