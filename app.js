
/**
 * QuestionDB Plugin
 */
(function (module){
  'use sctrict';
  /* globals module, require */
  
  var express = require('express')
  , fs = require("fs")
  , routes = require('./routes')
  , questiondb = require('./routes/questiondb')
  //, user = require('./routes/user')
  , path = require('path')
  , mongoose = require('mongoose')
  , user = module.parent.require('./user')
  , meta = module.parent.require('./meta')
  , db = module.parent.require('../src/database')
  , nconf = module.parent.require('nconf')
  , winston = module.parent.require('winston');
  
  var obj;
  fs.readFile('./config.json', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    //mongoose.connect('mongodb://'+obj.mongo.host+'/fg_qdb', { user: obj.mongo.username, pass: obj.mongo.password });
    winston.log("error", "QDBL u"+mongoose.connection.readyState)
  });
  
  var QuestionDB= {};
  QuestionDB.init = function(params, callback){
    /*function render(req, res){
      res.render()
    }*/
//    params.app.set('view engine', 'jade');
    params.router.get('/qdb', questiondb.list_tags);
    //params.router.get('/users', user.list);
    
    params.router.get('/qdb_sf', questiondb.show_form);
    params.router.post('/qdb_pf', questiondb.process_form);
    params.router.get('/qdb_l', questiondb.list_tags);
    params.router.get('/qdb_l/:tag', questiondb.list);
    params.router.get('/qdb_v/:qid', questiondb.view);
    params.router.get('/qdb_u/:qid', questiondb.show_form);
  }
  module.exports = QuestionDB;
}(module));

/*
var app = express();

// all environments

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.set('x-powered-by', false)
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}



http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
*/
